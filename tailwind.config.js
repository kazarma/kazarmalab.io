const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html"
  ],
  theme: {
    extend: {
      colors: {
        violet: "#650ef0",
        pink: "#ff2ef0",
        green: "#42d684",
        orange: "#ff7b52",
        beige: "#fffaf0"
      },
      typography: ({ theme }) => ({
        tsviolet: {
          css: {
            // '--tw-prose-body': theme('colors.violet'),
            '--tw-prose-headings': theme('colors.violet'),
            // '--tw-prose-lead': theme('colors.violet'),
            '--tw-prose-links': theme('colors.violet'),
            // '--tw-prose-bold': theme('colors.violet'),
            '--tw-prose-counters': theme('colors.violet'),
            '--tw-prose-bullets': theme('colors.violet'),
            '--tw-prose-hr': theme('colors.violet'),
            // '--tw-prose-quotes': theme('colors.violet'),
            '--tw-prose-quote-borders': theme('colors.violet'),
            // '--tw-prose-captions': theme('colors.violet'),
            // '--tw-prose-code': theme('colors.violet'),
            // '--tw-prose-pre-code': theme('colors.violet'),
            // '--tw-prose-pre-bg': theme('colors.violet'),
            '--tw-prose-th-borders': theme('colors.violet'),
            '--tw-prose-td-borders': theme('colors.violet'),

            // '--tw-prose-invert-body': theme('colors.violet'),
            // '--tw-prose-invert-headings': theme('colors.white'),
            // '--tw-prose-invert-lead': theme('colors.violet'),
            // '--tw-prose-invert-links': theme('colors.white'),
            // '--tw-prose-invert-bold': theme('colors.white'),
            // '--tw-prose-invert-counters': theme('colors.violet'),
            // '--tw-prose-invert-bullets': theme('colors.violet'),
            // '--tw-prose-invert-hr': theme('colors.violet'),
            // '--tw-prose-invert-quotes': theme('colors.violet'),
            // '--tw-prose-invert-quote-borders': theme('colors.violet'),
            // '--tw-prose-invert-captions': theme('colors.violet'),
            // '--tw-prose-invert-code': theme('colors.white'),
            // '--tw-prose-invert-pre-code': theme('colors.violet'),
            // '--tw-prose-invert-pre-bg': 'rgb(0 0 0 / 50%)',
            // '--tw-prose-invert-th-borders': theme('colors.violet'),
            // '--tw-prose-invert-td-borders': theme('colors.violet'),
          },
        },
      }),
    },
    fontFamily: {
      'sans': ['Roboto', ...defaultTheme.fontFamily.sans],
    }
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}


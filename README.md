Placeholder page for [https:/kazar.ma](https:/kazar.ma).

The `main` branch is automatically built and hosted at [https://kazarma.gitlab.io](https://kazarma.gitlab.io).

To run the developer setup:

```bash
npm install
npm run dev
```

To build for production:

```bash
npm install
npm run build
```
